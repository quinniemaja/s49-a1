import {Row, Col, Button} from 'react-bootstrap'

export default function Banner ({prop}){

	// const {header, p1, btnContent} = prop
	// console.log(header)

	return (
		<Row>
			<Col className = "p-5"> {/*for styling*/}
				<h1>{prop.header}</h1>
				<p>{prop.p1}</p>
				<Button variant = "primary">{prop.btnContent}</Button>
			</Col>
		</Row>

		)
}
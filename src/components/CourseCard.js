import {useState} from 'react'
import {Card, Button} from 'react-bootstrap'

export default function CourseCard ({courseProp}) {

	console.log(courseProp)

	// object deconstruction
	const {name, description, price} = courseProp

	// array deconstruction
	const [count, setCount] = useState(0)

	const [seat, seatCount] = useState(30)

	function enroll(){
		setCount(count +1);
		console.log("Enrollees" + count);

		seatCount(seat -1);
		console.log(seat)

		if (seat === 0 && count === 30){
			setCount(count +0)
			seatCount(seat -0)

			return alert('No more available seats')
		}


	}

	

	return (

	
		<Card className = "cardHighlight p-3">
			<Card.Body>
				<Card.Title>
					<h2>{name}</h2>
				</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>
					{description}
				</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Card.Text>Enrollees: {count}</Card.Text>
				<Button variant="primary" onClick = {enroll}>Enroll</Button>
			</Card.Body>
		</Card>
			
		
	)

	
}
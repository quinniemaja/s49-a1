import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {Redirect} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register () {

	const { user, setUser} = useContext(UserContext);

	//state hooks to store values of the input fields
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [email, setEmail] = useState('')

	//state to determine whether register button is enabled or not
	const [isActive, setIsActive] = useState(false)
	
	// console.log(email);
	// console.log(password1);
	// console.log(password2);

	function registerUser(e){

		//prevents page redirection via form submission
		e.preventDefault();


		localStorage.setItem('email', email);
		console.log(email);

		

		setEmail(''); //to clear input field after submission
		setPassword1('');
		setPassword2('');

		alert('You have successfully registered!')
	}

	 // Validation to enable submit button when all fields are populated and both passwords match
	useEffect(() => {
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {

			setIsActive(true)

		} else {

			setIsActive(false)
		}
	}, [email, password1, password2])

		console.log(user.email)
	return (

		(user.email !== null) ? 
		 <Redirect to = "/courses" />
		 :

		<Form onSubmit = {(e) => registerUser(e)}>
			<Form.Group>
				<Form.Label>Email Address:</Form.Label>
				<Form.Control 
					type = 'email'
					placeholder = 'Please enter your email here'
					value = {email}
					onChange = {e => setEmail(e.target.value)}
					required/>
				<Form.Text className = "text-muted">
					We'll never share your email with anyone else.
				</Form.Text>	
			</Form.Group>

			<Form.Group controlId = "password1">
				<Form.Label>Password:</Form.Label>
				<Form.Control 
					type = 'password'
					placeholder = 'Please input your password'
					value = {password1}
					onChange = {e => setPassword1(e.target.value)}
					required/>
			</Form.Group>

			<Form.Group controlId = "password2">
				<Form.Label>Verify Password:</Form.Label>
				<Form.Control 
					type = 'password'
					placeholder = 'Please verify your password'
					value = {password2}
					onChange = {e => setPassword2(e.target.value)}
					required/>
			</Form.Group>



			{ isActive ? // if statement
				<Button variant = 'primary' type = 'submit' id = 'submitBtn'>
							Register
				</Button>

				: // stands for else 
				<Button variant = 'danger' type = 'submit' id= 'submitBtn' disabled>
					Register
				</Button>
			}
		</Form>
	)
}
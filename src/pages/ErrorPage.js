import {Fragment} from 'react'
import Banner from '../components/Banner'

export default function ErrorPage () {

	const content = {
		header: "Error, Page Not Found",
		p1 : "Go back to homepage",
	 	btnContent : "Home Page",
	} 
	return (

		<Fragment>
			<Banner prop = {content}/>
		</Fragment>
	)
}
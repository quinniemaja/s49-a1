import {Fragment} from 'react'
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'


export default function Home (prop) {

	const content = {
		header: "Zuitt Coding Bootcamp",
		p1 : "Opportunities for everyone, everywhere!",
	 	btnContent : "Enroll Now!",
	}

	return (
		<Fragment>
			<Banner  prop = {content}/>
			<Highlights/>
			
		</Fragment>
	)
}
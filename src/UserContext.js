import React from 'react';

/*
a context is a special react object which will allows us to store information within and pass it around our components within the app.
The context object is a different approach to passing information between components without the need to pass from component to component. 

*/

const UserContext = React.createContext();

/*
	the "provider components allow other components to consume/use the necessarry information needed to the context objects"
*/
export const UserProvider = UserContext.Provider;

export default UserContext;